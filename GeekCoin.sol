pragma solidity ^0.4.18;

import "./ConvertLib.sol";
import "./StandardToken.sol";
import "./Ownable.sol";
import "./BondingCurveLin.sol";
import "./InflationaryToken.sol";

contract GEEKCoin is BondingCurveLin, InflationaryToken {
  using ConvertLib for uint;

  // uint public constant MAX_UINT = (2**256) - 1;

  string public constant name = "GeekToken";
  string public constant symbol = "GEEK";
  uint8 public constant decimals = 18;

  uint256 public constant TIME_INTERVAL = 1 hours;
  uint256 public constant HOURLY_INFLATION = 0; // 1000010880216701200 10% annual
  uint256 public constant INITIAL_SUPPLY = 0; // * (10 ** uint256(decimals))

  event Log(string logString, uint value);

  /**
   * @dev Not needed right now
   * @param addr that for which to check the balance
   * @return {uint} banace in Ether
   */
  function getBalanceInEth(address addr) public view returns(uint){
    return ConvertLib.convert(balanceOf(addr),2);
  }

  /**
   * @dev Constructor that gives msg.sender all of existing tokens.
   */
  function GEEKCoin() public {
    owner = msg.sender;

    // inflation params
    lastInflationCalc = now;
    inflationRatePerInterval = HOURLY_INFLATION;
    timeInterval = TIME_INTERVAL;

    // bonding curve params
    bondingCurveDecimals = decimals;
    dec = 10 ** uint256(bondingCurveDecimals);
    multiple = 100000000000000 ; //100000000000000 wei 0.0001 ether

    // token params
    totalSupply_ = INITIAL_SUPPLY;
    balances[owner] = INITIAL_SUPPLY;
    Transfer(0x0, owner, INITIAL_SUPPLY);
  }
  
  
     
    struct User {
        uint endorsement;
        address user_address;
    }
    
    
    event VotingResult(address user_address, uint endorsement, uint balance);
    
    mapping (address => User) public voters;
    User[] public users;

    
    function endorse(address endorsee){
        //@todo validate if user exists
        voters[endorsee].endorsement += balanceOf(msg.sender);
        VotingResult(endorsee, voters[endorsee].endorsement, balanceOf(msg.sender));
    }

}